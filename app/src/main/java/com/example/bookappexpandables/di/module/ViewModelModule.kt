package com.example.bookappexpandables.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.bookappexpandables.ui.bookdetail.BookViewModel
import com.example.bookappexpandables.ui.bookpager.BookDetailViewModel
import com.example.bookappexpandables.ui.categorylist.CategoryListViewModel
import com.example.bookappexpandables.viewmodel.AppViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(CategoryListViewModel::class)
    abstract fun bindCategoryListModel(categoryListViewModel: CategoryListViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(BookDetailViewModel::class)
    abstract fun bindBookDetailViewModel(bookDetailViewModel: BookDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BookViewModel::class)
    abstract fun bindBookViewModel(bookViewModel: BookViewModel): ViewModel

    @Binds
    abstract fun provideAppViewModelFactory(appViewModelFactory: AppViewModelFactory): ViewModelProvider.Factory

}