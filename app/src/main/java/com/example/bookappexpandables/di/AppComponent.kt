package com.example.bookappexpandables.di

import com.example.bookappexpandables.App
import com.example.bookappexpandables.di.module.BookDetailModule
import com.example.bookappexpandables.di.module.BookListModule
import com.example.bookappexpandables.di.module.CategoryListModule
import com.example.bookappexpandables.di.module.ViewModelModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AndroidSupportInjectionModule::class,
        CategoryListModule::class,
        ViewModelModule::class,
        BookListModule::class,
        BookDetailModule::class
    ]
)

interface AppComponent {

    fun inject(app: App)

}