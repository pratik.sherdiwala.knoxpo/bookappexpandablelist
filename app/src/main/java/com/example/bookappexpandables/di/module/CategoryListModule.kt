package com.example.bookappexpandables.di.module

import com.example.bookappexpandables.ui.categorylist.CategoryListActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class CategoryListModule {

    @ContributesAndroidInjector
    abstract fun contributeCategoryListActivity(): CategoryListActivity

}