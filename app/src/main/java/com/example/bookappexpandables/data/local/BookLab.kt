package com.example.bookappexpandables.data.local

import com.example.bookappexpandables.model.Book
import javax.inject.Inject

class BookLab @Inject constructor() {

    val thrillerList by lazy {

        val list = mutableListOf<Book>()

        list.add(
            Book(
                1,
                "The Girl in Room 105",
                "Chetan Bhagat",
                1
            )
        )

        list.add(
            Book(
                2,
                "Sherlock Holmes: The Definitive Collection",
                "Stephen Fry",
                1
            )
        )

        list.add(
            Book(
                3,
                "A Game of Thrones",
                "George R. R. Martin",
                1
            )
        )

        list.add(
            Book(
                4,
                "And Then There Were None",
                "Agatha Christie",
                1
            )
        )

        list.add(
            Book(
                5,
                "The World of Ice and Fire (Song of Ice & Fire)",
                "George R.R. Martin",
                1
            )
        )
        list
    }

    val actionList by lazy {

        val list = mutableListOf<Book>()

        list.add(
            Book(
                6,
                "Sherlock Holmes: The Truly Complete Collection",
                "Arthur Conan Doyle",
                2
            )
        )

        list.add(
            Book(
                7,
                "The Oath of the Vayuputras",
                "Amish Tripathi",
                2
            )
        )

        list.add(
            Book(
                8,
                "Treasure Island",
                "Robert Louis Stevenson",
                2
            )
        )
        list
    }

    val educationList by lazy {
        val list = mutableListOf<Book>()

        list.add(
            Book(
                9,
                "Kotlin Programming",
                "Iyanu Adelekan",
                3
            )
        )

        list.add(
            Book(
                10,
                "Android Cookbook: Problems and Solutions for Android Developers",
                "Ian F. Darwin",
                3
            )
        )

        list.add(
            Book(
                11,
                "Cracking the Coding Interview: 189 Programing Questions and Solutions",
                "Gayle Laakmann McDowell",
                3
            )
        )

        list.add(
            Book(
                12,
                "Alibaba: The House that Jack Ma Built",
                "Duncan Clark",
                3
            )
        )

        list
    }

    val romanceList by lazy {
        val list = mutableListOf<Book>()

        list.add(
            Book(
                13,
                "The Greatest Short Stories of Leo Tolstoy",
                "Leo Tolstoy",
                4
            )
        )

        list.add(
            Book(
                14,
                "The Perfect Us",
                "Durjoy Datta",
                4
            )
        )

        list.add(
            Book(
                15,
                "You are My Reason to Smile",
                "Arpit Vageria",
                4
            )
        )
        list

    }

    val sciFiList by lazy {

        val list = mutableListOf<Book>()

        list.add(
            Book(
                16,
                "Harry Potter and the Philosopher's Stone",
                "J.K. Rowling",
                5
            )
        )

        list.add(
            Book(
                17,
                "The Last Avatar (Age of Kalki #1)",
                "Vishwas Mudagal",
                5
            )
        )

        list.add(
            Book(
                18,
                "The Sparrow",
                "Mary Doria Russell",
                5
            )
        )

        list.add(
            Book(
                19,
                "Batman: Arkham Origins",
                "Ernest Hemingway",
                5
            )
        )

        list.add(
            Book(
                20,
                "Marvel's SPIDER-MAN: Hostile Takeove",
                "David Liss",
                5
            )
        )

        list.add(
            Book(
                21,
                "Avengers: Everybody Wants to Rule the World: A Novel of the Marvel Universe: 1",
                "Dan Abnett",
                5
            )
        )
        list
    }
}