package com.example.bookappexpandables.data.repository

import android.util.Log
import com.example.bookappexpandables.data.local.CategoryLab
import com.example.bookappexpandables.model.Book
import com.example.bookappexpandables.model.Category
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CategoryRepository @Inject constructor(
    private val categoryLab: CategoryLab,
    private val books: List<Book>
) {

    fun getCategories(): List<Category> {
        return categoryLab.categoryList
    }

    fun updateBook(book: Book, newName: String, newAuthor: String) {
        val index = books.indexOf(book)
        books[index].name = newName
        books[index].author = newAuthor
        Log.d("Repo", "$book")
    }
}