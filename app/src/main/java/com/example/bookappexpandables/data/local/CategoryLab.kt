package com.example.bookappexpandables.data.local

import com.example.bookappexpandables.model.Book
import com.example.bookappexpandables.model.Category
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CategoryLab @Inject constructor(
    private val books: List<Book>
) {

    /*
    private fun bookList(id: Int): List<Book> {
        val bookList = mutableListOf<Book>()
        for (book in books) {
            if (book.categoryId == id) {
                bookList.add(book)
            }
        }
        return bookList
    }
    */


    val categoryList by lazy {

        mutableListOf(
            Category(
                1,
                "Thriller",
                books.filter {
                    it.categoryId==1
                }
            ),
            Category(
                2,
                "Action",
                books.filter {
                    it.categoryId==2
                }
            ),
            Category(
                3,
                "Sci-Fi",
                books.filter {
                    it.categoryId==3
                }
            ),
            Category(
                4,
                "Education",
                books.filter {
                    it.categoryId==4
                }
            ),
            Category(
                5,
                "Romance",
                books.filter {
                    it.categoryId==5
                }
            )
        )
    }
}