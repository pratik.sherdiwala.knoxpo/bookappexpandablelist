package com.example.bookappexpandables.model

data class Category(
    val id: Int,
    val name: String,
    var books: List<Book>,
    var isExpanded: Boolean = false
)