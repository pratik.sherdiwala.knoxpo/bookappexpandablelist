package com.example.bookappexpandables.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Book(
    var id: Int,
    var name: String,
    var author: String,
    var categoryId: Int
) : Parcelable