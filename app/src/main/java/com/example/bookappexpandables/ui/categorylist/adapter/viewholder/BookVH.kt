package com.example.bookappexpandables.ui.categorylist.adapter.viewholder

import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.bookappexpandables.R
import com.example.bookappexpandables.model.Book

private val TAG = BookVH::class.java.simpleName

class BookVH(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val mNameTV = itemView.findViewById<TextView>(R.id.nameTV)
    private val mAuthorTV = itemView.findViewById<TextView>(R.id.authorTV)

    fun bindBook(
        book: Book,
        onBookSelected: (book: Book) -> Unit
    ) {

        mNameTV.text = book.name
        mAuthorTV.text = "-" + book.author

        itemView.setOnClickListener {
            onBookSelected(book)
            Log.d(TAG, "book : $book")
        }
    }
}