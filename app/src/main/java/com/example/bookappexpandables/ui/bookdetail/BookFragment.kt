package com.example.bookappexpandables.ui.bookdetail

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.bookappexpandables.R
import com.example.bookappexpandables.databinding.FragmentBookBinding
import com.example.bookappexpandables.model.Book
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class BookFragment : Fragment() {

    companion object {

        private val TAG = BookFragment::class.java.simpleName
        private val ARGS_BOOK = "$TAG.ARGS_BOOK"

        fun newInstance(book: Book) = BookFragment().apply {
            arguments = Bundle().apply {
                putParcelable(ARGS_BOOK, book)
            }
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentBookBinding

    private val viewModel by lazy {
        ViewModelProviders.of(
            this,
            viewModelFactory
        ).get(BookViewModel::class.java)
    }

    private val book: Book
        get() = arguments!!.getParcelable(ARGS_BOOK)!!

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        this.binding = FragmentBookBinding.inflate(
            inflater,
            container,
            false
        )

        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val fragment = this
        with(binding) {
            book = fragment.viewModel
        }
        viewModel.loadBook(book)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_book, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.updateBook -> {

                Log.d(TAG, "Book Fragment : $book")
                viewModel.updateBook(
                    book
                )
                activity?.onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}