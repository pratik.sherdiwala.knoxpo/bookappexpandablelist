package com.example.bookappexpandables.ui.categorylist

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.bookappexpandables.Event
import com.example.bookappexpandables.data.repository.CategoryRepository
import com.example.bookappexpandables.model.Book
import com.example.bookappexpandables.model.Category
import javax.inject.Inject

class CategoryListViewModel @Inject constructor(
    private val categoryRepository: CategoryRepository
) : ViewModel() {

    var itemList = MutableLiveData<List<Category>>()

    private val _openBookDetailEvent = MutableLiveData<Event<Book>>()
    val openBookDetailEvent: LiveData<Event<Book>>
        get() = _openBookDetailEvent

    fun getCategories() {
        itemList.value = categoryRepository.getCategories()
    }

    fun expandList(category: Category) {
        category.isExpanded = !category.isExpanded
        getCategories()
    }

    fun openBookDetail(book: Book) {
        _openBookDetailEvent.value = Event(book)
    }
}