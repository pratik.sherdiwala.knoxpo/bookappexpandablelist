package com.example.bookappexpandables.ui.categorylist.adapter.viewholder

import android.opengl.Visibility
import android.view.View
import android.widget.ImageButton
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.example.bookappexpandables.R
import com.example.bookappexpandables.model.Category

class CategoryVH(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val mId = itemView.findViewById<TextView>(R.id.categoryIdTV)
    private val mNameTV = itemView.findViewById<TextView>(R.id.nameTV)
    private val mArrowBTN = itemView.findViewById<ImageButton>(R.id.expandBTN)

    fun bindCategory(
        category: Category,
        categoryClicked: (category: Category) -> Unit
    ) {
        mId.text = category.id.toString()
        mNameTV.text = category.name

        itemView.setOnClickListener {
            categoryClicked(category)
            mArrowBTN.isVisible = !category.isExpanded
        }
    }
}
