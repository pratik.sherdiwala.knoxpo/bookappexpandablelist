package com.example.bookappexpandables.ui.categorylist.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.bookappexpandables.R
import com.example.bookappexpandables.model.Book
import com.example.bookappexpandables.model.Category
import com.example.bookappexpandables.ui.categorylist.CategoryListViewModel
import com.example.bookappexpandables.ui.categorylist.adapter.viewholder.BookVH
import com.example.bookappexpandables.ui.categorylist.adapter.viewholder.CategoryVH
import java.lang.IllegalArgumentException

private const val VIEW_TYPE_CATEGORY = 0
private const val VIEW_TYPE_BOOK = 1

class CategoryAdapter(
    private val categoryListViewModel: CategoryListViewModel
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items: List<Any>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when (viewType) {
            VIEW_TYPE_CATEGORY -> {
                return CategoryVH(
                    LayoutInflater.from(
                        parent.context
                    ).inflate(
                        R.layout.item_category_list,
                        parent,
                        false
                    )
                )
            }

            VIEW_TYPE_BOOK -> {
                return BookVH(
                    LayoutInflater.from(
                        parent.context
                    ).inflate(
                        R.layout.item_book_list,
                        parent,
                        false
                    )
                )
            }
            else -> throw IllegalArgumentException()
        }
    }

    override fun getItemCount() = items?.size ?: 0

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is CategoryVH -> holder.bindCategory(items!![position] as Category) {
                categoryListViewModel.expandList(it)
            }
            is BookVH -> holder.bindBook(items!![position] as Book) {
                categoryListViewModel.openBookDetail(it)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (items?.get(position)) {
            is Category -> VIEW_TYPE_CATEGORY
            is Book -> VIEW_TYPE_BOOK

            else -> throw IllegalArgumentException()
        }
    }

    fun updateItem(categories: List<Category>) {
        val newItems = mutableListOf<Any>()
        categories.forEach {
            newItems.add(it)
            if (it.isExpanded) {
                newItems.addAll(it.books)
            }
        }
        items = newItems
        notifyDataSetChanged()
    }
}