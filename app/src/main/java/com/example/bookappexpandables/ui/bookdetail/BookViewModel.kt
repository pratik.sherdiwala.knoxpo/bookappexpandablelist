package com.example.bookappexpandables.ui.bookdetail

import android.util.EventLog
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.bookappexpandables.Event
import com.example.bookappexpandables.data.repository.CategoryRepository
import com.example.bookappexpandables.model.Book
import javax.inject.Inject

private val TAG = BookViewModel::class.java.simpleName

class BookViewModel @Inject constructor(
    private val categoryRepository: CategoryRepository
) : ViewModel() {

    val name = MutableLiveData<String>()
    val author = MutableLiveData<String>()

    fun loadBook(book: Book) {
        if (name.value == null && author.value == null) {
            name.value = book.name
            author.value = book.author
        }
    }

    fun updateBook(book: Book) {
        Log.d(TAG, "book VM: $book")

        categoryRepository.updateBook(
            book
            , name.value!!
            , author.value!!
        )
    }
}