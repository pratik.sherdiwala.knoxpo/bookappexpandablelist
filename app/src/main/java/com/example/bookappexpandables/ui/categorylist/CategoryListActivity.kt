package com.example.bookappexpandables.ui.categorylist

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.animation.AnimationUtils
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.bookappexpandables.R
import com.example.bookappexpandables.databinding.ActivityCategoryListBinding
import com.example.bookappexpandables.model.Book
import com.example.bookappexpandables.ui._common.DataBindingActivity
import com.example.bookappexpandables.ui.bookpager.BookDetailActivity
import com.example.bookappexpandables.ui.categorylist.adapter.CategoryAdapter
import dagger.android.AndroidInjection
import javax.inject.Inject


private val TAG = CategoryListActivity::class.java.simpleName

class CategoryListActivity : DataBindingActivity<ActivityCategoryListBinding>(), Navigation {

    private val REQUEST_UPDATE = 1

    override val layoutId = R.layout.activity_category_list

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by lazy {
        ViewModelProviders.of(
            this,
            viewModelFactory
        )
            .get(CategoryListViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        val activity = this

        with(binding) {
            lifecycleOwner = activity
            viewModel = activity.viewModel

            categoryRV.layoutManager = LinearLayoutManager(activity)
            categoryRV.layoutAnimation =
                AnimationUtils.loadLayoutAnimation(activity, R.anim.layout_animator)
            categoryRV.adapter = CategoryAdapter(activity.viewModel)

            swipeRefreshLayout.setOnRefreshListener {
                swipeRefreshLayout.isRefreshing = false
            }
        }
        viewModel.getCategories()

        with(viewModel) {
            openBookDetailEvent.observe(activity, Observer {
                it?.getContentIfNotHandled()?.let {
                    activity.openBookDetail(it)
                }
            })
        }
    }

    override fun openBookDetail(book: Book) {
        startActivityForResult(
            BookDetailActivity.intentForOpenBookDetail(this, book),
            REQUEST_UPDATE
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_UPDATE -> {
                if (resultCode == Activity.RESULT_OK) {
                    Log.d(TAG, "Activity Res")
                    viewModel.getCategories()
                }
            }
        }
    }
}