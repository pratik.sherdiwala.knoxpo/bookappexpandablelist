package com.example.bookappexpandables.ui._common

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.bookappexpandables.model.Category
import com.example.bookappexpandables.ui.categorylist.adapter.CategoryAdapter

object DataBindingAdapter {

    @JvmStatic
    @BindingAdapter("items")
    fun setItems(view: RecyclerView, items: List<Category>?) {
        items?.let {
            (view.adapter as CategoryAdapter).updateItem(it)
        }
    }
}